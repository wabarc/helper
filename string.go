// Copyright 2021 Wayback Archiver. All rights reserved.
// Use of this source code is governed by the MIT
// license that can be found in the LICENSE file.

package helper // import "github.com/wabarc/helper"

import (
	"math/rand"
)

func RandString(length int, letter string) string {
	alphabet := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	switch letter {
	case "capital", "upper":
		alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	case "lower":
		alphabet = "abcdefghijklmnopqrstuvwxyz"
	}

	bytes := make([]byte, length)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphabet[b%byte(len(alphabet))]
	}

	return string(bytes)
}
